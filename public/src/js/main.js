console.log("main.js loaded ⚙️");

const resultsSection = document.querySelector(".results");
const movieOutput = document.querySelector(".movie__output");
const moviePoster = document.querySelector(".movie__poster");
const searchField = document.querySelector(".search__field");
const searchButton = document.querySelector(".search__button");

let getSearchString = function() {
  let searchTerm = searchField.value.trim();
  searchTerm.replace(" ", "+");
  movieSearch(searchTerm);
  searchField.value = "";
};

searchButton.addEventListener("click", getSearchString);

document.addEventListener("keydown", function(event) {
  if (event.keyCode === 13 || event.which === 13) {
    console.log("Enter pressed");
    getSearchString();
  }
});

let movieSearch = function(title) {
  // Clear results section from old search querys
  resultsSection.innerHTML = "";

  // Gets all movies by search string
  fetch(`https://www.omdbapi.com/?apikey=b14715dc&s=${title}&type=Movie`)
    .then(response => {
      if (response.ok) return response.json();
      else console.log("Ooops! Something went wrong 💩");
    })
    .then(handleData = (results) => {
      // Outputs every found movie as result in div
      for (let movie in results.Search) {
        movie = results.Search[movie];

        // Save movie title als url-friendly query
        let urlTitle = movie.Title.replace(/ /g, "-").toLowerCase();

        let poster = function() {
          if (movie.Poster === "N/A") {
            return "<p>Sorry, no poster for this movie available!</p><br />";
          } else {
            return `<img class="movie__poster responsive" src="${movie.Poster}" alt="${movie.Title}" />`
          }
        }           
        resultsSection.insertAdjacentHTML("beforeend", `
          <div class="movie">
            <div class="movie__output">
              <h2 class="movie__title">${movie.Title} (${movie.Year})</h2>` + poster() +
              `<p><a class="btn" href="/movie/${urlTitle}-${movie.imdbID}">More info...</a></p>
            </div>
          </div>`);
      };
    })
};

let movieDetails = function(title) {
  fetch(`https://www.omdbapi.com/?apikey=thewdb&t=${title}`)
    .then(response => {
      if (response.ok) return response.json();
      else console.log("Ooops! Something went wrong 💩");
    })
    .then(handleData = (movie) => {
      movieOutput.innerHTML += `<h2>${movie.Title}</h2>`;

      for (let info in movie) {
        if (info === "Poster") {
          moviePoster.innerHTML +=
          `
          <img src="${movie[info]}" alt="${movie.Title} Poster"/>
          `
        } else {
        movieOutput.innerHTML += 
        `
        <p><strong>${info}:</strong> ${movie[info]}</p>
        `
        }
      };
    })
};
