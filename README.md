# simple-movie-database

Search for great movies with this simple movie database I've created to test fetch &amp; promises in JS

[![Netlify Status](https://api.netlify.com/api/v1/badges/1756814f-855f-4c33-957e-faa3ff94f5b0/deploy-status)](https://app.netlify.com/sites/simple-movie-database/deploys)
