/*  ================
        APP INIT
    ================  */

const express = require("express");
const app = express();

// Loads ejs files without .ejs extension
app.set("view engine", "ejs");

// Set root directory for express app
app.use(express.static(__dirname + "/public"));

/*  ==============
        ROUTES
    ==============  */

app.get("/", (req, res) => {
    res.render("main");
});

app.get("/movie/:id", (req, res) => {
    let url = req.params.id;
    res.render("details", {url: url});
});

app.get("*", (req, res) => {
    res.send("<h1>404 - Ouch!</h1><p>The site you're looking for is not available (yet)!</p>");
});

/*  ====================
        START SERVER    
    ====================  */
    
app.listen(3000, "localhost", () => {
    console.log("Server started on 127.0.0.1:3000 💻");
});